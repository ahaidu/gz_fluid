find_package(Doxygen)

if (DOXYGEN_FOUND)
  configure_file(${CMAKE_SOURCE_DIR}/doc/FluidPlugin.in
                 ${CMAKE_BINARY_DIR}/FluidPlugin.dox @ONLY)

  add_custom_target(doc

    # Generate the API documentation
    ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/FluidPlugin.dox
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMAND cp ${CMAKE_SOURCE_DIR}/doc/search.js
            ${CMAKE_BINARY_DIR}/doxygen/html/search
    COMMAND make -C ${CMAKE_BINARY_DIR}/doxygen/latex
    COMMAND mv ${CMAKE_BINARY_DIR}/doxygen/latex/refman.pdf
            ${CMAKE_BINARY_DIR}/doxygen/latex/FluidPlugin.pdf

    COMMENT "Generating API documentation with Doxygen" VERBATIM)
endif()

