#include "gz_test_problem.cc"
#include <gtest/gtest.h>
 
TEST(APlusBTest, PositiveNos) { 
    ASSERT_EQ(3, aPlusB(1,2));
	ASSERT_EQ(0, aPlusB(0,0));
}
 
TEST(APlusBTest, NegativeNos) {
    ASSERT_EQ(-3, aPlusB(-1,-2));
	ASSERT_EQ(0, aPlusB(-1,1));
}
 
int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
